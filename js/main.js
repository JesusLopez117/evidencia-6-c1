//Función para limpiar las cajas de texto
const limpiar = () =>{
    document.getElementById("identi").value = "";
    document.getElementById("nombre").value = "";
    document.getElementById("nombreUs").value = "";
    document.getElementById("email").value = "";
    document.getElementById("domicilio").value = "";
    document.getElementById("calle").value = "";
    document.getElementById("numero").value = "";
    document.getElementById("ciudad").value = "";
}

//Función para realizar la petición
function cargarAjax(){
    const url = "https://jsonplaceholder.typicode.com/users"; //URL del sitio a consumir

    axios
    .get(url) //Se obtiene la información del sitio consumido
    .then((res)=>{  //El Axios nos permite obtener la información transformada en Json
        mostrar(res.data)

    })
    .catch((err =>{
        console.log("Surgio un error" + Error);
    }
    ))

    function mostrar(data){
        const id = document.getElementById("identi").value;
        if(id != ""){   //Validación para campo ID vacio.
            for(item of data){
                if(id == item.id){
                    document.getElementById("nombre").value = item.name;
                    document.getElementById("nombreUs").value = item.username;
                    document.getElementById("email").value = item.email;
                    document.getElementById("calle").value = item.address.street;
                    document.getElementById("numero").value = item.address.suite;
                    document.getElementById("ciudad").value = item.address.city;
                }
            }

            if(document.getElementById("nombre").value == ""){  //Validación de ID incorrecto. Logica: si nombre no esta pintado, entonces no lo encontro.
                alert("El Id ingresado es incorrecto");
                limpiar();
            }

        }else{
            alert("El campo Id se ecuentra vacio");
        }
    }
}

//Botones
document.getElementById("btnLimpiar").addEventListener("click", limpiar);
document.getElementById("btnBuscar").addEventListener("click", function(){
    cargarAjax();
});